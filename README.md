# example-test

basic example test for java

# Steps
   * clone the repository (git clone git@gitlab.com:lsalvatierrasoliz/example-test.git)
   * create a new branch aclaure, rvillca
   * follow exercise 1 and exercise 2 instructions
   * commit and push your changes once done (at your branch). Dont touch master branch !



# exercise 1

   * Implement the service/UserService interface in order to provide crud operations
    * initialize the userservice with five users by default. each one of these five users should have a unique id.
   * model/User must have id, username, email, phone, gender, address.
   * optional you can complete, implement findByGender method for UserService interface, but is not required.
   * CRUD operations
    * create user operation must generate a new unique key id for it
    * update user operation just must update user info like phone, address, email and gender. username is not updatable


# exercise 2

  * Problem

    Se dice que una matriz con un número impar de elementos está centrada si todos los elementos (excepto el del medio) son estrictamente mayores que el valor del elemento medio. Tenga en cuenta que solo las matrices con un número impar de elementos tienen un elemento intermedio. Escriba una función que acepte una matriz de enteros y devuelva 1 si se trata de una matriz centrada; de lo contrario, devuelve 0.

    | input array  | return                                                                 |
    | ------------ |--------------------------------------------------------------------    |
    | {8, 9, 7, 10, 11}|  1 (elemento del medio 7 es estrictamente menor que los otros elementos )|
    | {3, 6, 3, 4, 5}  |  0 (elemento del medio 3 no es estrictamente menor que los otros elementos)|
    | {1, 2, 3, 4}  |  0 (no hay elemento medio)|
    | {}  | 0 (no hay elemento medio) |
    | {9}  | 1 (al ser el unico elemento medio es considerada centrada) |



   Please add that function to Main class

