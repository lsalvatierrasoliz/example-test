package com.company;

import com.company.controller.UserController;
import com.company.model.Gender;
import com.company.model.User;
import com.company.service.impl.UserServiceImpl;

public class Main {

    public static void main(String[] args) {
	    //--------------------- exercise 1 -------------------//
        UserController controller = new UserController(new UserServiceImpl());

        User user1 = new User("plazarte", "sucre", Gender.MALE, "plazarte@yopmail.com", "591- 4263878");
        User user2 = new User("pricaldi", "la paz", Gender.MALE, "pricaldi@yopmail.com", "591- 4263878");
        User user3 = new User("rpena", "pacata baja", Gender.MALE, "rpena@yopmail.com", "591- 4263878");

        // create users
        user1 = controller.createUser(user1);
        user2 = controller.createUser(user2);
        user3 = controller.createUser(user3);


        /// udpate users
        User user4 = new User(user3.getUsername(), "pacata alta", user3.getGender(), user3.getEmail(), "8000100003");
        controller.updateUser(user3.getId(), user4);

        // find user
        User findUser =  controller.getUserById(user1.getId());


        // delete user3 rpena
        controller.deleteUser(user3.getId());

        //list all users
        controller.getAllUsers().forEach(user -> System.out.println("username : " +user.getUsername() + "userid : " + user.getId()));


        //--------------------- exercise 2 -------------------//
        int[] a = {1,2,3,4,5};
        int[] b = {3,2,1,4,5};
        int[] c = {3,2,1,4,1};
        int[] d = {1,2,3,4};
        int[] e = {};
        int[] f = {10};

        System.out.println("Centered Value  : "  +isCenteredOddArray(a));
        System.out.println("Centered Value  : "  +isCenteredOddArray(b));
        System.out.println("Centered Value  : "  +isCenteredOddArray(c));
        System.out.println("Centered Value  : "  +isCenteredOddArray(d));
        System.out.println("Centered Value  : "  +isCenteredOddArray(e));
        System.out.println("Centered Value  : "  +isCenteredOddArray(f));

    }

    public static int isCenteredOddArray(int[] numbers){
        if(null == numbers || numbers.length % 2 == 0) return 0;

        int middleIndex = numbers.length / 2;
        int middleElement = numbers[middleIndex];

        for(int i = 0; i < numbers.length; i++ ){
            if(i != middleIndex && middleElement >= numbers[i]){
                return 0;
            }
        }

        return 1;
    }

}
