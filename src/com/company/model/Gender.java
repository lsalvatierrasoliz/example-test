package com.company.model;

/**
 * Created by lsalvatierra on 8/9/2018.
 */
public enum Gender {
    FEMALE, MALE
}
