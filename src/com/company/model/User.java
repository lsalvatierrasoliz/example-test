package com.company.model;

/**
 * Created by lsalvatierra on 8/9/2018.
 */
public class User {

    private Integer id;
    private String username;
    private String address;
    private Gender gender;
    private String email;
    private String phone;

    public User(String username, String address,  Gender gender, String email, String phone){
        this.username = username;
        this.address = address;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


}
