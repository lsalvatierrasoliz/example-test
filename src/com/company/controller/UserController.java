package com.company.controller;

import com.company.model.User;
import com.company.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsalvatierra on 8/9/2018.
 */
public class UserController {

    private UserService userService;

    public UserController(UserService userService){
        this.userService = userService;
    }


    public User createUser(User user){
        return userService.save(user);
    }

    /***
     *  TODO : user proper service call to filter users by gender
     *  search users by Gender
     * @return
     */
    public List<User> searchUserByGender(){
        return new ArrayList<User>();
    }

    public User updateUser(Integer userId, User user){
        return userService.update(userId, user);
    }

    public void deleteUser(Integer userId){
        userService.remove(userId);
    }

    public List<User> getAllUsers(){
        return userService.findAll();
    }

    public User getUserById(Integer userId){
        return userService.findById(userId);
    }
}
