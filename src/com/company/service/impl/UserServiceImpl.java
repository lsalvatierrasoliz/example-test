package com.company.service.impl;

import com.company.model.Gender;
import com.company.model.User;
import com.company.service.UserService;

import java.util.*;

/**
 * Created by lsalvatierra on 8/9/2018.
 */
public class UserServiceImpl implements UserService {

    private static final Map<Integer, User> users = new HashMap<Integer, User>();

    static {

        User a = new User("lsalvatierra", "quillacollo", Gender.MALE, "lsalvatierra@yopmail.com", "591- 4263878");
        a.setId(1);

        User b = new User("aclaure", "pacata", Gender.MALE, "aclaure@yopmail.com", "591- 4263878");
        b.setId(2);

        User c = new User("rvillca", "pacata", Gender.MALE, "rvillca@yopmail.com", "591- 4263878");
        c.setId(3);

        User d = new User("aclaure", "chimba", Gender.MALE, "aclaure@yopmail.com", "591- 4263878");
        d.setId(4);

        User e = new User("slima", "oruro", Gender.FEMALE, "slima@yopmail.com", "591- 4263878");
        e.setId(5);

        users.put(1 , a);
        users.put(2 , b);
        users.put(3 , c);
        users.put(4 , d);
        users.put(5 , e);

    }

    @Override
    public User save(User user) {
        if(null == user){
            throw new RuntimeException("Unable to create user. User object is null");
        }

        Integer userId = getMaxUserId() + 1;
        user.setId(userId);

        users.put(userId, user);
        return user;

    }

    @Override
    public User findById(Integer userId) {
        return users.get(userId);
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(users.values());
    }

    @Override
    public void remove(Integer userId) {
        if(null == userId) return;


        users.remove(userId);
    }

    @Override
    public User update(Integer userId, User user) {
       if(null == userId || null == findById(userId)) {
           throw  new RuntimeException("Unable to update user. Id for user is null or user does not exists");
       }

       User userUpdate = findById(userId);
       if(null != user){
           userUpdate.setAddress(user.getAddress());
           userUpdate.setGender(user.getGender());
           userUpdate.setEmail(user.getEmail());
           userUpdate.setPhone(user.getPhone());
       }


        users.put(userId, userUpdate);
        return userUpdate;
    }

    protected Integer getMaxUserId()

    {

        Set<Integer> keys = users.keySet();
        Integer maxId = 1;

        for (Integer key : keys) {
            if(key > maxId) {
                maxId = key;
            }
        }

        return maxId;
    }
}
