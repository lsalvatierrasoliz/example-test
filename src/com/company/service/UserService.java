package com.company.service;

import com.company.model.User;

import java.util.List;

/**
 * Created by lsalvatierra on 8/9/2018.
 */
public interface UserService {

    User save(User user);
    User findById(Integer userId);
    List<User> findAll();

    void remove(Integer userId);
    User update(Integer userId, User user);

    /// TODO: complete sign of method in order to filter users by gender
    //List<User> findByGender();

}
